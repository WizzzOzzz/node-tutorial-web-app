const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port  = process.env.PORT || 3000;
var app = express();

hbs.registerPartials(__dirname + "/views/partials");
// hbs.registerPartials(__dirname + "/imgs");
hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

app.use((req, res, next) =>{
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`
  var fileName = 'server.log'
  fs.appendFile(fileName , log + '\n', (err) => {
    if (err){
      console.log(`Unable to append to ${fileName}`);
    }
  });
  console.log(log);
  next();
})

// app.use((req, res, next) => {
//   if (req.url === '/'){
//     res.render('template-info.hbs',{
//       tabTitle: "maintnence",
//       pageTitle: "The page is under Maintnence",
//       // welcomeMessage: 'Nothing to see here!'
//     });
//   } else {
//     next();
//   }
// });

hbs.registerHelper('screamIt', (str) => {
  return str.toUpperCase();
})

// app.use(express.static(__dirname + "/public"));

app.get('/', (req, res) => {
  res.render('template-info.hbs',{
    // menu: '<p><a href="/">Home</a></p><p><a href="/about">About</a></p><p><a href="/help">Help</a></p>',
    image: './img/Home-Wallpaper-20.jpg',
    tabTitle: "Home",
    pageTitle: "Homepage",
    p: 'Welcome To My Website!',
    homeActive: 'active'
  });
});

app.get('/about', (req, res) => {
  res.render('template-info.hbs',{
    // menu: '<p><a href="/">Home</a></p><p><a href="/about">About</a></p><p><a href="/help">Help</a></p>',
    image: './img/about_us.jpg',
    tabTitle: "About Me",
    pageTitle: "About Me",
    p: 'The paragraph is about me!',
    aboutActive: 'active'
  });
});

app.get('/project', (req, res) => {
  res.render('template-info.hbs',{
    // menu: '<p><a href="/">Home</a></p><p><a href="/about">About</a></p><p><a href="/help">Help</a></p>',
    image:'./img/project.jpg',
    tabTitle: "Project",
    pageTitle: "My project:",
    p: 'This is my project!',
    projectActive: 'active'
  });
});

app.get('/help', (req, res) => {
  res.render('template-info.hbs',{
    // menu: '<p><a href="/">Home</a></p><p><a href="/about">About</a></p><p><a href="/help">Help</a></p>',
    image: './img/help.png',
    tabTitle: "Help page",
    pageTitle: "This Is The Help Page:",
    p: 'Help is on it\'s way!',
    helpActive: 'active'
  });
});

app.get('/bad', (req, res) => {
  res.status(404).send({
    // menu: '<p><a href="/">Home</a></p><p><a href="/about">About</a></p><p><a href="/help">Help</a></p>',
    errorMessage: "That's the worst it would get!",
    status: 'NOT_FOUND',
    code: 404
  });
});

app.listen(port, () => {
  console.log(`server is app and running on port: ${port}`)
});
